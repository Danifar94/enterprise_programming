﻿using RealEstateGitHub.ActionFilters;
using RealEstateGitHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace RealEstateGitHub.Controllers
{
    public class PropertyController : Controller
    {
        private PropertyModel getPropertyDetail(int? id = null, String locality = null, String propertyType = null)
        {
            if (id.HasValue)
            {
                return new PropertyModel(id.Value)
                {
                    Locality = locality,
                    PropertyType = propertyType,
                    Bedrooms = 3,
                    ContractType = "Sale",
                    ImageURL = "listing.jpg",
                    Description = "Very bright and spacious, second floor apartment being sold furnished, forming part of a modern block of apartments, " +
                              "served with a lift, situated in close proximity of both Sliema and St.Julians' centre.This property measuring " +
                              "approximately 80 sqm consists of a spacious kitchen, living and dining area, a main bedroom with shower ensuite " +
                              "and another spacious double bedroom, guest bathroom, washroom and two balconies.Ideal as a short let or long let " +
                              "rental investment. Worth viewing."
                };
            }
            else
            {
                return new PropertyModel()
                {
                    Locality = locality,
                    PropertyType = propertyType,
                    Bedrooms = 3,
                    ContractType = "Sale",
                    ImageURL = "listing.jpg",
                    Description = "Very bright and spacious, second floor apartment being sold furnished, forming part of a modern block of apartments, " +
                              "served with a lift, situated in close proximity of both Sliema and St.Julians' centre.This property measuring " +
                              "approximately 80 sqm consists of a spacious kitchen, living and dining area, a main bedroom with shower ensuite " +
                              "and another spacious double bedroom, guest bathroom, washroom and two balconies.Ideal as a short let or long let " +
                              "rental investment. Worth viewing."
                };
            }
        }

        // GET: Search
        [HttpGet]   // Modifies the Search to respond to HTTP Get verbs
        [ActionName("Search")] // This method will respond to action "Search" even though it has a different name
        // Cache the output for 5 minutes (300 seconds) on both the server and the client.  A different cached version should be
        // kept for each locality and propertyType
        [OutputCache(Location = OutputCacheLocation.ServerAndClient, Duration = 300, VaryByParam ="locality;propertyType")]
        public ActionResult Filter(String locality = null, String propertyType = null)
        {
            IList<PropertyModel> searchResults = new List<PropertyModel>();

            for (int i = 0; i < 9; i++)
            {
                searchResults.Add(
                        getPropertyDetail(i, locality, propertyType) 
                    );
            }

            // both propertyType and locality are available!
            if (propertyType != null && locality != null)
            {
                ViewBag.Title = propertyType + " in " + locality;
                ViewBag.Header = propertyType + " in " + locality;
            } else if (locality != null) // only locality is available!
            {
                ViewBag.Title = "Property in " + locality;
                ViewBag.Header = "Property in " + locality;
            } else if (propertyType != null) // only propertyType is available!
            {
                ViewBag.Title = propertyType;
                ViewBag.Header = propertyType;
            } else // neither of the parameters are available!
            {
                ViewBag.Title = "Property Search";
                ViewBag.Header = "Property Search";
            }

            return View(searchResults);
        }

        [HttpGet] // This action will respond to HTTP Get
        public ActionResult PropertyDetail(int? id)
        {
            return View(getPropertyDetail(id));
        }

        [HttpPost] // This action will respond to HTTP Posts
        public ActionResult PropertyDetail(PropertyModel property)
        {
            throw new NotImplementedException("ToDo: Implement the action!");
        }

        // [Exception] - no longer needed as now it is globally registered
        public ActionResult NotImplementedAction()
        {
            throw new NotImplementedException("ToDo: Implement the action!");
        }
    }
}