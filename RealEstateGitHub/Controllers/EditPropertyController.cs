﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using RealEstateGitHub.Models;

namespace RealEstateGitHub.Controllers
{
    public class EditPropertyController : Controller
    {
        private RealEstateDBContext db = new RealEstateDBContext();

        // GET: EditProperty
        public async Task<ActionResult> Index()
        {
            return View(await db.Property.ToListAsync());
        }

        // GET: EditProperty/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropertyModel propertyModel = await db.Property.FindAsync(id);
            if (propertyModel == null)
            {
                return HttpNotFound();
            }
            return View(propertyModel);
        }

        // GET: EditProperty/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EditProperty/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Reference,Locality,PropertyType,Bedrooms,ContractType,Description,FloorArea,Price,ImageURL")] PropertyModel propertyModel)
        {
            if (ModelState.IsValid)
            {
                db.Property.Add(propertyModel);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(propertyModel);
        }

        // GET: EditProperty/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropertyModel propertyModel = await db.Property.FindAsync(id);
            if (propertyModel == null)
            {
                return HttpNotFound();
            }
            return View(propertyModel);
        }

        // POST: EditProperty/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Reference,Locality,PropertyType,Bedrooms,ContractType,Description,FloorArea,Price,ImageURL")] PropertyModel propertyModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(propertyModel).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(propertyModel);
        }

        // GET: EditProperty/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PropertyModel propertyModel = await db.Property.FindAsync(id);
            if (propertyModel == null)
            {
                return HttpNotFound();
            }
            return View(propertyModel);
        }

        // POST: EditProperty/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            PropertyModel propertyModel = await db.Property.FindAsync(id);
            db.Property.Remove(propertyModel);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
