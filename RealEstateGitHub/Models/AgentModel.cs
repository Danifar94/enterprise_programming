﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealEstateGitHub.Models
{
    public class AgentModel
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int AgentID { get; set; }

        [Required]
        public String Name { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public String PhoneNumber { get; set; }

        [Required]
        public String Email { get; set; }

        /// <summary>
        /// Virtual is used for Lazy Loading. The Agent details are not loaded, unless required.
        /// Since an Agent can have multiple properties, then ICollection can be used.
        /// </summary>
        [Display(Name = "Agent Property")]
        public virtual ICollection<PropertyModel> Properties { get; set; }
    }
}