﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace RealEstateGitHub.Models
{
    public class ExceptionDetail
    {
        public Exception Exception { get; set; }
        public DateTime Time { get; set; }
        public IPrincipal User { get; set; }
    }
}