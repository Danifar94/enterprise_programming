namespace RealEstateGitHub.Migrations
{
    using RealEstateGitHub.Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<RealEstateGitHub.Models.RealEstateDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "RealEstateGitHub.Models.RealEstateDBContext";
        }

        protected override void Seed(RealEstateGitHub.Models.RealEstateDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            var propertyList = new List<PropertyModel>
            {
                new PropertyModel() { Locality="Paola", PropertyType="Maisonette", ContractType="Sale", Bedrooms=2, FloorArea=110, Price=180000, ImageURL="listing.jpg" },
                new PropertyModel() { Locality="Hamrun", PropertyType="Apartment", ContractType="Sale", Bedrooms=3, FloorArea=105, Price=170000, ImageURL="listing.jpg" },
                new PropertyModel() { Locality="Marsa", PropertyType="Apartment", ContractType="Let", Bedrooms=2, FloorArea=90, Price=650, ImageURL="listing.jpg" },
                new PropertyModel() { Locality="Paola", PropertyType="Maisonette", ContractType="Sale", Bedrooms=2, FloorArea=110, Price=180000, ImageURL="listing.jpg" },
                new PropertyModel() { Locality="Paola", PropertyType="Maisonette", ContractType="Sale", Bedrooms=2, FloorArea=110, Price=180000, ImageURL="listing.jpg" }
            };

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            propertyList.ForEach(p => context.Property.AddOrUpdate(p));
            context.SaveChanges();

            base.Seed(context);
        }
    }
}
