﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RealEstateGitHub
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Maps url like:
            // ~/Property/Apartment-in-Paola
            routes.MapRoute(
                name: "PropertySearchPropertyTypeLocality",
                url: "Property/{propertyType}-in-{locality}",
                defaults: new { controller = "Property", action = "Search" }
            );

            // Sets the default action in Property to Search
            routes.MapRoute(
                name: "PropertyDefault",
                url: "Property/{action}",
                defaults: new { controller = "Property", action = "Search", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
