﻿using RealEstateGitHub.ActionFilters;
using System.Web;
using System.Web.Mvc;

namespace RealEstateGitHub
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ExceptionAttribute());  // globally register ExceptionAttribute
        }
    }
}
