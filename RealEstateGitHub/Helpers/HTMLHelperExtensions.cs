﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstateGitHub.Helpers
{
    public static class HTMLHelperExtensions
    {
        public static MvcHtmlString DateTimePicker(this HtmlHelper helper, string id)
        {
            TagBuilder outerDivTag = new TagBuilder("div");
            outerDivTag.MergeAttribute("style", "overflow:hidden;");

            TagBuilder formGroupTag = new TagBuilder("div");
            formGroupTag.AddCssClass("form-group");

            TagBuilder rowTag = new TagBuilder("div");
            rowTag.AddCssClass("row");

            TagBuilder colTag = new TagBuilder("div");
            colTag.AddCssClass("col-md-8");

            TagBuilder pickerTag = new TagBuilder("div");
            pickerTag.GenerateId(id);

            // nest the tags within each other
            colTag.InnerHtml = pickerTag.ToString();
            rowTag.InnerHtml = colTag.ToString();
            formGroupTag.InnerHtml = rowTag.ToString();

            outerDivTag.InnerHtml += formGroupTag.ToString();

            TagBuilder scriptTag = new TagBuilder("script");
            scriptTag.MergeAttribute("type", "text/javascript");
            scriptTag.InnerHtml = "$(function () { " +
                                  "$('#" + TagBuilder.CreateSanitizedId(id) + "').datetimepicker({ " +
                                  "daysOfWeekDisabled: [0, 6], " +
                                  "inline: true, " +
                                  "sideBySide: true " +
                                  "});" +
                                  "});";

            outerDivTag.InnerHtml += scriptTag.ToString();

            return MvcHtmlString.Create(outerDivTag.ToString());
            /*
              <div style="overflow:hidden;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="bookViewingDateTimePicker"></div>
                        </div>
                    </div>
                </div>
        
                <script type="text/javascript">
                    $(function () {
                        $('#bookViewingDateTimePicker').datetimepicker({
                            daysOfWeekDisabled: [0, 6],
                            inline: true,
                            sideBySide: true
                        });
                    });
                </script>
        
            </div>
           */
        }
    }
}