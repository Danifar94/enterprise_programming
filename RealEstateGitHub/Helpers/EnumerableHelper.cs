﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RealEstateGitHub.Helpers
{
    public static class EnumerableHelper
    {
        /// <summary>
        /// This extension method, enables us to obtain a number of items (defined by the parameter size) at a time from this instance of IEnumerable
        /// </summary>
        /// <typeparam name="TSource">The type of data being stored in the IEnumerable being batched.</typeparam>
        /// <param name="source">The IEnumerable to batch into small groups.</param>
        /// <param name="size">The size of each batch.</param>
        /// <returns>The data stored in the original IEnumerable as batches of type IEnumerable.</returns>
        public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(
                  this IEnumerable<TSource> source, int size)
        {
            if (size <= 0)
            {
                throw new ArgumentOutOfRangeException("size", size, "Size of batch must be greater than 0!");
            }

            TSource[] bucket = null;
            var count = 0;

            foreach (var item in source)
            {
                if (bucket == null)
                    bucket = new TSource[size];

                bucket[count++] = item;
                if (count != size)
                    continue;

                yield return bucket;    // return the next batch, using the yield return keyword,
                                        // enabling the batches to be built one at a time as they are
                                        // being used.

                bucket = null;
                count = 0;
            }

            if (bucket != null && count > 0)
                yield return bucket.Take(count);    // same as above, yield return the last few items.
        }
    }
}