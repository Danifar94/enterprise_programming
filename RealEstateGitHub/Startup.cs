﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RealEstateGitHub.Startup))]
namespace RealEstateGitHub
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
