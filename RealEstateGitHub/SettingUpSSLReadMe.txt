To set up SSL, perform the following steps:

1) Go to the Project in the Solution Explorer, go to the Properties tab and SSL Enabled to true.

2) Copy the SSL URL to the Project Properties, and replace the Project URL with the SSL URL.

3) Add the [RequireHttps] tag to the Controller and Actions related to account management and where
   SSL is required (example, when you have sensitive data).

4) Install an SSL certificate to be used with the website.