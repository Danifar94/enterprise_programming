﻿1) Go to:
Tools > NuGet Package Manager > Package Manager Console

2) Execute:
Install-Package Bootstrap.v3.Datetimepicker
Install-Package Bootstrap.v3.Datetimepicker.CSS

3) You still require to add the bootstrap collapse and transition javascript files!
Make sure to update your references to include the newly installed js and css files!
