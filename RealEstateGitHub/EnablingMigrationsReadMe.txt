﻿1) Go to:
Tools > NuGet Package Manager > Package Manager Console

2) Write the following command:
enable-migrations -ContextTypeName RealEstateGitHub.Models.RealEstateDBContext

Note: You may replace the Context above with the context for which you want to enable Data Migration.

Note: If you have an existing database, then the schema of the database will be used as the initial one.

3) If you update the model, then you can call the following commands:
add-migration AddAgentsTable
update-database

Note: You can replace the name AddAgentsTable to the migration name that you wish.

4) To Rollback to a previous Data Migration, you can use the update command as follows:
Update-Database -TargetMigration:"InitialCreate"

Note: Replace the InitialCreate with the name of the migration you want to use.
Note: If no target migration name is used, the update will take the database to the last version.
