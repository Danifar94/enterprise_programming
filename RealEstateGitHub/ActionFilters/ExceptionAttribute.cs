﻿using RealEstateGitHub.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RealEstateGitHub.ActionFilters
{

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class ExceptionAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (filterContext.Exception != null)
            {
                IList<ExceptionDetail> exceptionList;

                HttpContext.Current.Application.Lock();     // Application Context is locked, and only 1 thread can access this code

                try
                {
                    if (HttpContext.Current.Application["ExceptionList"] != null)
                    {
                        exceptionList = (IList<ExceptionDetail>) HttpContext.Current.Application["ExceptionList"];
                    }
                    else
                    {
                        exceptionList = new List<ExceptionDetail>();

                        HttpContext.Current.Application.Add("ExceptionList", exceptionList);
                    }

                    var exceptionDetail = new ExceptionDetail()
                    {
                        Exception = filterContext.Exception,
                        Time = DateTime.Now,
                        User = filterContext.HttpContext.User
                    };

                    exceptionList.Add(exceptionDetail);
                }
                finally // make sure that the ApplicationState is unlocked!
                {
                    HttpContext.Current.Application.UnLock();
                }
            }

            base.OnActionExecuted(filterContext);
        }
    }
}
